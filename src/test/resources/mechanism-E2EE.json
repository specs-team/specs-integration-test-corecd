{
  "security_mechanism_id" : "E2EE",
  "security_mechanism_name" : "e2ee",
  "sm_description" : "...",
  "security_capabilities" : [
    "specs:e2ee"
  ],
  "enforceable_metrics" : [
    "write_serializability_M17",
    "read_freshness_M18",
    "confidentiality_M19",
    "integrity_M25"
  ],
  "monitorable_metrics" : [
    "confidentiality_M19",
    "specs:client_code:E2EE_MSR1"
  ],
  "measurements" : [{
    "msr_id" : "specs:client_code:E2EE_MSR1",
    "msr_description" : "Availability of the latest version of the Client code at the web store.",
    "frequency" : "24h",
    "metrics" : [
      "confidentiality_M19"
    ],
    "monitoring_event" : {
      "event_id" : "specs:client_code_not_available:E2EE_E1",
      "event_description" : "Client code is not available.",
      "event_type" : "VIOLATION",
      "condition" : {
        "operator" : "eq",
        "threshold" : "no"
      }
    }
  }
  ],
  "metadata" : {
    "components" : [{
      "component_name" : "E2EE_Server",
      "component_type" : "server",
      "cookbook" : "E2EE",
      "recipe" : "install_E2EE_server",
      "implementation_step" : 1,
      "pool_seq_num" : 1,
      "vm_requirement" : {
        "hardware" : "m1.large",
        "usage" : "50",
        "acquire_public_ip" : "false",
        "private_ips_count" : 1,
        "firewall" : {
          "incoming" : {
            "source_ips" : [],
            "source_nodes" : [],
            "interface" : "private:1",
            "proto" : [
              "TCP"
            ],
            "port_list" : [
              "22",
              "80",
              "443",
              "8080"
            ]
          },
          "outcoming" : {
            "destination_ips" : [],
            "destination_nodes" : [],
            "interface" : "private:1",
            "proto" : [
              "TCP"
            ],
            "port_list" : [
              "*"
            ]
          }

        }
      }
    }
    ],
    "constraints" : [{
      "ctype" : "SC1a",
      "arg1" : [
        "e2ee_main_server"
      ],
      "arg2" : [
        "e2ee_main_db",
        "e2ee_backup_server",
        "e2ee_backup_db",
        "e2ee_auditor",
        "e2ee_monitoring_adapter"
      ]
    }, {
      "ctype" : "SC1a",
      "arg1" : [
        "e2ee_main_db"
      ],
      "arg2" : [
        "e2ee_backup_server",
        "e2ee_backup_db",
        "e2ee_auditor",
        "e2ee_monitoring_adapter"
      ]
    }, {
      "ctype" : "SC1a",
      "arg1" : [
        "e2ee_backup_server"
      ],
      "arg2" : [
        "e2ee_backup_db",
        "e2ee_auditor",
        "e2ee_monitoring_adapter"
      ]
    }, {
      "ctype" : "SC1a",
      "arg1" : [
        "e2ee_backup_db"
      ],
      "arg2" : [
        "e2ee_auditor",
        "e2ee_monitoring_adapter"
      ]
    }, {
      "ctype" : "SC1a",
      "arg1" : [
        "e2ee_auditor"
      ],
      "arg2" : [
        "e2ee_monitoring_adapter"
      ]
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_main_server"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_main_db"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_backup_server"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_backup_db"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_auditor"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC2a_1",
      "arg1" : [
        "e2ee_monitoring_adapter"
      ],
      "op" : "=",
      "n1" : "1"
    }, {
      "ctype" : "SC3",
      "n1" : "5"
    }
    ]
  },
  "remediation" : {
    "remediation_actions" : [{
      "name" : "e2ee_a1",
      "action_description" : "Upload the latest version of the E2EE Client to the web store and check its availability.",
      "recipes" : [
        "e2ee_r1",
        "e2ee_r2"
      ]
    }
    ],
    "remediation_flow" : [{
      "name" : "specs:client_code_not_available:E2EE_E1",
      "action_id" : "e2ee_a1",
      "yes_action" : "observe",
      "no_action" : "notify"
    }
    ]
  },
  "chef_recipes" : [{
    "name" : "e2ee_r1",
    "recipe_description" : "Upload the latest version of the Client component to the Chrome web store.",
    "associated_metrics" : [
      "confidentiality_M19"
    ],
    "associated_measurements" : [
      "specs:client_code:E2EE_MSR1"
    ],
    "dependent_components" : []
  }, {
    "name" : "dbb_r1",
    "recipe_description" : "Install Main Server.",
    "associated_metrics" : [
      "write_serializability_M17",
      "read_freshness_M18",
      "integrity_M25"
    ],
    "associated_measurements" : [
      "specs:primary_server_availability:DBB_MSR9"
    ],
    "dependent_components" : [
      "DBB_main_server"
    ]
  }, {
    "name" : "dbb_r2",
    "recipe_description" : "Install Main DB.",
    "associated_metrics" : [
      "write_serializability_M17",
      "read_freshness_M18",
      "integrity_M25"
    ],
    "associated_measurements" : [
      "specs:primary_db_availability:DBB_MSR10"
    ],
    "dependent_components" : [
      "DBB_main_db"
    ]
  }, {
    "name" : "dbb_r3",
    "recipe_description" : "Install Backup Server.",
    "associated_metrics" : [
      "write_serializability_M17",
      "read_freshness_M18",
      "integrity_M25"
    ],
    "associated_measurements" : [
      "specs:backup_server_availability:DBB_MSR11"
    ],
    "dependent_components" : [
      "DBB_backup_server"
    ]
  }, {
    "name" : "dbb_r4",
    "recipe_description" : "Install Backup DB.",
    "associated_metrics" : [
      "write_serializability_M17",
      "read_freshness_M18",
      "integrity_M25"
    ],
    "associated_measurements" : [
      "specs:backup_db_availability:DBB_MSR12"
    ],
    "dependent_components" : [
      "DBB_backup_db"
    ]
  }, {
    "name" : "dbb_r5",
    "recipe_description" : "Install Auditor.",
    "associated_metrics" : [
      "write_serializability_M17",
      "read_freshness_M18",
      "integrity_M25"
    ],
    "associated_measurements" : [],
    "dependent_components" : [
      "DBB_auditor"
    ]
  }, {
    "name" : "dbb_r6",
    "recipe_description" : "Install Monitoring Adapter.",
    "associated_metrics" : [],
    "associated_measurements" : [],
    "dependent_components" : [
      "DBB_monitoring_adapter"
    ]
  }
  ]
}

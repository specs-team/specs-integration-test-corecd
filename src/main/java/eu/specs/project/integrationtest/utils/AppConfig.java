package eu.specs.project.integrationtest.utils;

import org.apache.commons.io.IOUtils;

import java.io.StringReader;
import java.util.Properties;

public class AppConfig {
    private static final String SPECS_PLATFORM_IP_VAR = "SPECS_PLATFORM_IP";

    private String specsPlatformIp;
    private String slaManagerApiAddress;
    private String serviceManagerApiAddress;
    private String sloManagerAddress;
    private String planningApiAddress;
    private String implementationApiAddress;
    private String diagnosisApiAddress;
    private String rdsApiAddress;
    private String monipoliAddress;
    private String eventHubAddress;

    public AppConfig(String propertiesFile) throws Exception {
        String propertiesString = IOUtils.toString(this.getClass().getResourceAsStream(propertiesFile));
        if (propertiesString.contains(template(SPECS_PLATFORM_IP_VAR)) && System.getenv(SPECS_PLATFORM_IP_VAR) == null) {
            throw new Exception(String.format("Environment variable %s is not set.", SPECS_PLATFORM_IP_VAR));
        }
        specsPlatformIp = System.getenv(SPECS_PLATFORM_IP_VAR);
        propertiesString = propertiesString
                .replace(template(SPECS_PLATFORM_IP_VAR), specsPlatformIp);

        Properties properties = new Properties();
        properties.load(new StringReader(propertiesString));

        this.slaManagerApiAddress = properties.getProperty("sla-manager-api.address");
        this.serviceManagerApiAddress = properties.getProperty("service-manager-api.address");
        this.sloManagerAddress = properties.getProperty("slo-manager-api.address");
        this.planningApiAddress = properties.getProperty("planning-api.address");
        this.implementationApiAddress = properties.getProperty("implementation-api.address");
        this.diagnosisApiAddress = properties.getProperty("diagnosis-api.address");
        this.rdsApiAddress = properties.getProperty("rds-api.address");
        this.monipoliAddress = properties.getProperty("monipoli.address");
        this.eventHubAddress = properties.getProperty("event-hub.address");
    }

    public String getSpecsPlatformIp() {
        return specsPlatformIp;
    }

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getServiceManagerApiAddress() {
        return serviceManagerApiAddress;
    }

    public String getSloManagerAddress() {
        return sloManagerAddress;
    }

    public String getPlanningApiAddress() {
        return planningApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public String getDiagnosisApiAddress() {
        return diagnosisApiAddress;
    }

    public String getRdsApiAddress() {
        return rdsApiAddress;
    }

    public String getMonipoliAddress() {
        return monipoliAddress;
    }

    public String getEventHubAddress() {
        return eventHubAddress;
    }

    private static String template(String var) {
        return "${" + var + "}";
    }
}

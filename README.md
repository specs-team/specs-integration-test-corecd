# Integration scenarios CoreCD

This family of scenarios integrates components that combine monitoring and remediation functionalities. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-CD1
This scenario merges the Core-C2 and Core-D1 integration scenarios. Involved components enable the monitoring and remediation steps.

Details:
- Base Scenario ID: Core-C2, Core-D1
- Added artefacts: /

Involved components:
- SLA Platform:	/
- Negotiation module: /
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /